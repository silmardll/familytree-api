# Familytree Project

#### Ask for the `development.key` file
- Paste it inside `config/credentials/` directory

#### Ask for the `test.key` file
- Paste it inside `config/credentials/` directory

#### Docker configuration:
- docker and docker-compose must be installed
- `cp .env/development/database.example .env/development/database`
- `cp .env/development/web.example .env/development/web`
- `docker-compose build`
- `docker-compose up -d`

#### Run test suite
- `docker-compose exec web rails db:prepared`
- `docker-compose exec rake`

#### The API doc can be find it at:
- http://localhost:3000/api-docs
