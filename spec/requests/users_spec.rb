# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "Sign up users", type: :request do
  let(:default_params) do
    {
      data: {
        email:    "mail@mail.com",
        password: "1234567890"
      }
    }
  end

  path "/users" do
    post "creates a user" do
      tags "Users"
      consumes "application/json"
      parameter name: :default_params, in: :body, schema: {
        type:       :object,
        required:   ["data"],
        properties: {
          data: {
            type:       :object,
            required:   %w[email password],
            properties: {
              email:    { type: :string, example: "mail@mail.com" },
              password: { type: :string, example: "2wsx1qaz" }
            }
          }
        }
      }

      produces "application/json"

      response 201, "created" do
        schema(
          type:       :object,
          required:   ["data"],
          properties: {
            data: {
              type:       :object,
              required:   %w[id email],
              properties: {
                id:    { type: :integer, example: 1 },
                email: { type: :string,  example: "mail@mail.com" }
              }
            }
          }
        )

        run_test!

        it "returns just two keys inside data key" do
          expect(json_body["data"].keys.size).to eq 2
        end
      end

      response 422, "unprocessable entity" do
        before :each do
          default_params[:data].merge!(password: nil)
        end

        schema(
          type:       :object,
          required:   ["errors"],
          properties: {
            errors: {
              type:  :array,
              items: {
                type:       :object,
                required:   %w[type message],
                properties: {
                  type:    { type: :string, example: "blank" },
                  message: { type: :string, example: "Password can't be blank" }
                }
              }
            }
          }
        )

        run_test!
      end
    end
  end
end
