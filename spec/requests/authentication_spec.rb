# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "authentication", type: :request do
  let(:user) { create(:user) }
  let(:default_params) do
    {
      data: {
        email:    user.email,
        password: user.password
      }
    }
  end

  path "/auth/login" do
    post "Sign in user" do
      tags "Login"
      consumes "application/json"
      parameter name: :default_params, in: :body, schema: {
        type:       :object,
        properties: {
          data: {
            type:       :object,
            required:   %w[email password],
            properties: {
              email:    { type: :string, example: "mail@mail.com" },
              password: { type: :string, example: "1qaz2wsx" }
            }
          }
        }
      }

      produces "application/json"

      response(201, "created") do
        schema(
          type:       :object,
          required:   ["data"],
          properties: {
            data: {
              type:       :object,
              required:   ["auth_token"],
              properties: {
                auth_token: { type: :string, example: "1qazxsw2.1qazxsw2..." }
              }
            }
          }
        )

        run_test!
      end

      response 401, "unauthorized" do
        before :each do
          default_params[:data].merge!(password: nil)
        end

        schema(
          type:       :object,
          required:   ["errors"],
          properties: {
            errors: {
              type:  :array,
              items: {
                type:       :object,
                required:   %w[type message],
                properties: {
                  type:    { type: :string, example: "unauthorized" },
                  message: { type: :string, example: "unauthorized" }
                }
              }
            }
          }
        )

        run_test!
      end
    end
  end
end
