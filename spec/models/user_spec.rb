# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_length_of(:password).is_at_least(6).is_at_most(16) }

  it "has a valid factory" do
    expect(user).to be_valid
  end

  describe '#email' do
    it "invalid with a non-standard format" do
      user.email = 'mail.com'
      expect(user).to_not be_valid
    end

    it 'valid with a standard format' do
      user.email = 'mail@mail.com'
      expect(user).to be_valid
    end
  end

  context "has a uniqueness email" do
    subject { build(:user, email: "mail@mail.com") }
    it { should validate_uniqueness_of(:email).case_insensitive }
  end
end
