module RequestSpecHelper
  def json_body
    JSON.parse(response.body)
  end

  def do_request(extra_parameters = {})
    message = <<-STR
      missing method or uri for #{description}, method: #{method}, uri: #{uri}
    STR
    raise message unless method && uri

    parameters = default_params || {}
    parameters.merge!(extra_parameters)

    default_headers = { "Content-Type" => "application/json" }
    headers         = default_headers.merge(headers || {})

    send(method, uri, params: parameters.to_json, headers: headers)
  end

  private

  def example
    self.class
  end

  def description
    example.metadata[:parent_example_group][:description_args].first
  end

  def method
    example.metadata[:method]
  end

  def uri
    example.metadata[:uri]
  end
end

RSpec.configure do |config|
  config.include RequestSpecHelper, type: :request
end
