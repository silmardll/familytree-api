# frozen_string_literal: true

require "rails_helper"

RSpec.describe JsonWebToken do
  let(:token) { JsonWebToken.encode(default_paylod) }
  let(:default_paylod) { { id: 1 } }

  it "responds to HMAC_SECRET constant" do
    expect(JsonWebToken::HMAC_SECRET).to_not be_nil
  end

  context "default expiration time" do
    it "24 hours from now" do
      start_range = 23.hours.from_now + 59.minutes + 0.seconds
      end_range   = 24.hours.from_now +  1.minutes + 0.seconds
      expect(JsonWebToken::DEFAULT_EXP).to be_between(start_range, end_range)
    end
  end

  describe ".encode" do
    context "when payload is not set" do
      let(:default_paylod) { nil }

      it "generates a valid token" do
        expect(token).to_not be_nil
      end
    end

    context "when exp argument is not sent" do
      it "generates a valid token" do
        expect(token).to_not be_nil
      end
    end
  end

  describe ".decode" do
    context "with a valid token" do
      let(:body) { JsonWebToken.decode(token) }

      it "gets the body correctly" do
        expect(body).to include(:id)
      end

      it "must be an instance of HashWithIndifferentAccess" do
        expect(body).to be_instance_of(HashWithIndifferentAccess)
      end
    end

    context "with an invalid token" do
      it "raises an JWT::DecodeError" do
        token = "eyJhbGciOiJIUz.bVsbA.hJtQ9mvc7-RZgePFzh_ded3p1XfilDmfi1IE"
        expect { JsonWebToken.decode(token) }
          .to raise_error JWT::DecodeError, /Invalid segment encoding/
      end
    end
  end
end
