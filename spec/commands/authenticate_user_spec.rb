require "rails_helper"

RSpec.describe AuthenticateUser do
  let(:command) { described_class.call(user.email, user.password) }
  let(:user) { create(:user) }

  describe "#call" do
    context "with valid credentials" do
      it "generates an auth token" do
        expect(command.result).to_not be_nil
      end
    end

    context "with invalid credentials" do
      it "does not generate an auth token" do
        user.email    = "mail@mail.com"
        user.password = "1qaz2wsx"
        expect(command.result).to be_nil
      end
    end
  end

  describe "#result" do
    context "with valid credentials" do
      it "returns auth token" do
        expect(command.result).to_not be_nil
      end
    end

    context "with invalid credentials" do
      it "must be nil" do
        user.email    = "mail@mail.com"
        user.password = "1qaz2wsx"
        expect(command.result).to be_nil
      end
    end
  end

  describe "#success?" do
    context "with valid credentials" do
      it "must be success" do
        expect(command).to be_success
      end
    end

    context "with invalid credentials" do
      it "must not be success" do
        user.email    = "mail@mail.com"
        user.password = "1qaz2wsx"
        expect(command).to_not be_success
      end
    end
  end
end
