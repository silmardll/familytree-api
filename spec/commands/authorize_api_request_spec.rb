require "rails_helper"

RSpec.describe AuthorizeApiRequest do
  before :each do
    @user   = create(:user)
    @scheme = "Token"
    @token  = JsonWebToken.encode(user_id: @user.id)
  end

  let(:command) { described_class.call(headers) }
  let(:headers) do
    { "Authorization" => "#{@scheme} #{@token}" }
  end

  describe "#call" do
    context "with a valid scheme and token" do
      it "finds the user" do
        expect(command.result).to eq @user
      end
    end

    context "with a missing scheme and token" do
      before :each do
        @scheme = nil
        @token  = nil
      end

      it "generates a 'Schema missing' error message" do
        expect(command.errors.full_messages).to include("Scheme missing")
      end

      it "generates a 'Token missing' error message" do
        expect(command.errors.full_messages).to include("Token missing")
      end

      it "generates just thow error messages" do
        expect(command.errors.full_messages.count).to eq 2
      end
    end

    context "with an invalid scheme" do
      before :each do
        @scheme = "Bearer"
      end

      it "generates an 'Schema not supported; supported schemes: Token' "\
         "error message" do
        expect(command.errors.full_messages)
          .to include("Scheme not supported; supported schemes: Token")
      end

      it "generates just one error message" do
        expect(command.errors.full_messages.count).to eq 1
      end
    end

    context "with a valid scheme and a missing token" do
      before :each do
        @token = nil
      end

      it "generates a 'Token missing' error message" do
        expect(command.errors.full_messages).to include("Token missing")
      end

      it "generates just one error message" do
        expect(command.errors.full_messages.count).to eq 1
      end
    end

    context "with a valid scheme and an expired token" do
      it "generates an 'Invalid token signature has expired' error message" do
        @token = JsonWebToken.encode({ user_id: @user.id }, 1.minutes.ago)
        expect(command.errors.full_messages)
          .to include("Invalid token signature has expired")
      end
    end

    context "with a valid scheme and invalid token" do
      before :each do
        @token = "1qaz<F2>2wsx3ed4rfv5tg.1qaz2wsx3ed"
      end

      it "does not find the user" do
        expect(command.result).to be_nil
      end

      it "generates an 'Invalid token' error message" do
        expect(command.errors.full_messages)
          .to include("Invalid token not enough or too many segments")
      end
    end
  end

  describe "#result" do
    context "with a valid scheme and token" do
      it "returs the user" do
        expect(command.result).to eq @user
      end
    end

    context "with a valid scheme and an invalid token" do
      it "does not return the user" do
        @token = "1qaz<F2>2wsx3ed4rfv5tg.1qaz2wsx3ed"
        expect(command.result).to be_nil
      end
    end
  end

  describe "#success?" do
    context "with a valid scheme and token" do
      it "must be success" do
        expect(command).to be_success
      end
    end

    context "with a valid scheme and an invalid token" do
      it "must not be success" do
        @token = "1qaz<F2>2wsx3ed4rfv5tg.1qaz2wsx3ed"
        expect(command).to_not be_success
      end
    end
  end
end
