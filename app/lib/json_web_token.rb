# frozen_string_literal: true

class JsonWebToken
  HMAC_SECRET = Rails.application.credentials.secret_key_base
  DEFAULT_EXP = 24.hours.from_now

  def self.encode(payload, exp = DEFAULT_EXP)
    payload.merge!(exp: exp.to_i) if payload.present?
    JWT.encode(payload, HMAC_SECRET)
  end

  def self.decode(token)
    body = JWT.decode(token, HMAC_SECRET).first
    HashWithIndifferentAccess.new(body)
  end
end
