# frozen_string_literal: true

module SimpleCommand
  extend ActiveSupport::Concern
  include ActiveModel::Validations

  module ClassMethods
    # The call method of a SimpleCommand should always return itself
    def call(*args)
      new(*args).tap(&:call)
    end
  end

  def call
    raise NotImplementedError
  end

  def result
    raise NotImplementedError
  end

  def success?
    errors.none?
  end
end
