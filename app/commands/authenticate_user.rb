# frozen_string_literal: true

class AuthenticateUser
  include SimpleCommand

  validate :validate_credentials

  def initialize(email, password)
    @email    = email
    @password = password
    @token    = nil
  end

  def call
    @token = JsonWebToken.encode(user_id: user.id) if valid?
  end

  def result
    @token
  end

  private

  attr_reader :email, :password

  def validate_credentials
    return if user&.authenticate(password)

    errors.add(:user_authentication, "invalid credentials")
  end

  def user
    @user ||= User.find_by(email: email)
  end
end
