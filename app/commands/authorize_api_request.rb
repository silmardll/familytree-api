# frozen_string_literal: true

class AuthorizeApiRequest
  include SimpleCommand

  SUPPORTED_SCHEMES = %w[Token].freeze

  validate :validate_scheme_and_token_presence
  validate :validate_scheme,
    if: -> { scheme.present? }
  validate :validate_token,
    if: -> { scheme.present? && token.present? }

  def initialize(headers)
    @headers = headers
  end

  def call
    user if valid?
  end

  def result
    @user
  end

  private

  attr_reader :headers, :scheme, :token

  def validate_scheme_and_token_presence
    @scheme, @token = authorization_header&.split(" ")

    errors.add(:scheme, "missing") unless @scheme
    errors.add(:token, "missing") unless @token
  end

  def validate_scheme
    return if SUPPORTED_SCHEMES.include?(scheme)

    message = "not supported; supported schemes: #{SUPPORTED_SCHEMES.join}"
    errors.add(:scheme, message)
  end

  def validate_token
    decoded_auth_token
  end

  def user
    @user ||= User.find(decoded_auth_token[:user_id])
  end

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(token)
  rescue JWT::DecodeError => e
    errors.add(:invalid_token, e.message.downcase)
    nil
  end

  def authorization_header
    @authorization_header ||= headers["Authorization"]
  end
end
