# frozen_string_literal: true

class AuthenticationController < ApplicationController
  def authenticate
    command = AuthenticateUser.call(auth_params[:email], auth_params[:password])

    if command.success?
      render json: { data: { auth_token: command.result } }, status: :created
    else
      render json:   { errors: generate_response_errors(command) },
             status: :unauthorized
    end
  end

  private

  def auth_params
    params.require(:data).permit(:email, :password)
  end
end
