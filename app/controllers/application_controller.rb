# frozen_string_literal: true

class ApplicationController < ActionController::API
  def generate_response_errors(record)
    record.errors.details.each_with_object([]) do |(key, value), errors|
      errors << {
        type:    value.first[:error].to_s,
        message: record.errors[key].join
      }
    end
  end
end
