# frozen_string_literal: true

class UsersController < ApplicationController
  def create
    user = User.new(user_params)

    if user.save
      render json: UserBlueprint.render(user, root: :data), status: :created
    else
      render json:   { errors: generate_response_errors(user) },
             status: :unprocessable_entity
    end
  end

  private

  def user_params
    params.require(:data).permit(:email, :password)
  end
end
