# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  validates :email,
    presence:   true,
    email:      true,
    uniqueness: { case_sensitive: false }

  validates :password,
    length: { in: 6..16 },
    if:     -> { password.present? }
end
