class EmailValidator < ActiveModel::EachValidator
  FORMAT = /\A[-\w]+(\.[-\w]+)*@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/

  def validate_each(record, attribute, value)
    unless value =~ FORMAT
      record.errors.add(attribute, :invalid)
    end
  end
end
