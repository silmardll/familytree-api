FROM ruby:2.7.1-alpine3.12

RUN apk add --update --no-cache \
  build-base \
  postgresql-dev \
  tzdata \
  && rm -rf /var/cache/apk/*

WORKDIR ./usr/src/app

COPY Gemfile* ./

RUN bundle install

COPY . .

EXPOSE 3000

CMD ["bin/rails", "s", "-b", "0.0.0.0"]
