mime_types = %w[
  application/json
  application/vnd.familytree.v1+json
]

Mime::Type.unregister :json
Mime::Type.register "application/json", :json, mime_types
