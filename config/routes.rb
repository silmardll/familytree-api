Rails.application.routes.draw do
  mount Rswag::Api::Engine => "/api-docs"
  mount Rswag::Ui::Engine => "/api-docs"

  post "/auth/login",
    to: "authentication#authenticate"

  resources :users,
    only: [:create]
end
